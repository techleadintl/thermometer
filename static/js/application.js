
$(document).ready(function(){

    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/thermo');
    //var socket = io.connect('http://192.168.9.208:8085/test');
    var numbers_received = [];
    socket.on('newnumber', function(msg)
    {

        var vals =JSON.parse(msg.number1);
        numbers_received.push(msg.number1);
        console.log(numbers_received);
        var table = document.getElementById("tbl");

        if(numbers_received.length >= 100){
            numbers_received.pop();
            document.getElementById("tbl").deleteRow(numbers_received.length);
        }

        var row = table.insertRow(1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);

        cell1.innerHTML = vals['temp'];
        cell2.innerHTML = vals['hum'];
        cell3.innerHTML = new Date().toLocaleString();

        var page_height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        var headerHeight = document.getElementById("header_1").offsetHeight;
        var panelHeight = document.getElementById("panel_1").offsetHeight;
        var tableHeaderHeight = document.getElementById("tableheader").offsetHeight;
        var footerHeight = document.getElementById("footer").offsetHeight;
        var my_height = page_height - (headerHeight + panelHeight + tableHeaderHeight + footerHeight +50);


        document.getElementById('tableBodyHeight').style.height = my_height + "px";
        document.getElementById('temp').value = vals['temp'];
        document.getElementById('hum').value = vals['hum'];

    });

});
