import socket
import threading
import time
import sys
from umodbus.client.serial import rtu
import requests
import json
from umodbus.client.serial.redundancy_check import get_crc, validate_crc
from umodbus.functions import (create_function_from_response_pdu,
                               expected_response_pdu_size_from_request_pdu,
                               pdu_to_function_code_or_raise_error, ReadCoils,
                               ReadDiscreteInputs, ReadHoldingRegisters,
                               ReadInputRegisters, WriteSingleCoil,
                               WriteSingleRegister, WriteMultipleCoils,
                               WriteMultipleRegisters)

from umodbus.utils import recv_exactly
import smtplib


head = ['TEMP','HUMIDITY']

formatter = ""
for x in head:
    formatter+="{: >40} "

print(formatter.format(*head))

def send_message(adu, serial_port):

    """ Send ADU over serial to to server and return parsed response.

    :param adu: Request ADU.
    :param sock: Serial port instance.
    :return: Parsed response from server.
    """
    serial_port.send(adu)

    #time.sleep(0.05)

    # Check exception ADU (which is shorter than all other responses) first.
    exception_adu_size = 5

    response_error_adu = recv_exactly(serial_port.recv, exception_adu_size)
    #print(response_error_adu)

    rtu.raise_for_exception_adu(response_error_adu)

    expected_response_size = \
        expected_response_pdu_size_from_request_pdu(adu[1:-2]) + 3
    response_remainder = recv_exactly(
        serial_port.recv, expected_response_size - exception_adu_size)
   
    return rtu.parse_response_adu(response_error_adu + response_remainder, adu)

import smtplib

from email.mime.text import MIMEText

gmail_user = 'metertechlead@gmail.com'  
gmail_password = 'Mtr@123!'
to = ['nishad.janake@technomediclk.com'] 
def alert(temp):   

    subject = 'Alert!'
    body = 'Alert! The temperature has exceeded your threshold limit! Temperature recorded - ' + str(temp)
    message = 'Subject: {}\n\n{}'.format(subject, body)

    try:  
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        server.sendmail(gmail_user, to, message)
        server.close()

        print( 'Email sent!')
    except Exception as e:  
        print ('Something went wrong while sending the email...', e)
        pass

def sendSms(temp):
    print('sending sms..')
    try:
        body = 'Alert! The temperature has exceeded your threshold limit! Temperature recorded - ' + str(temp)
        response = requests.get('http://apps.bellvantage.com:8049/SendSMS.asmx/Send?UserName=Lush&Password=Sms%40009%23hsuL&AccountNumber=23&Message='+body+'&MessageTo=%2B94775546352')
        print(response)
    except:
        print('error sending sms')
        pass
    

values=[0,0]
def handle_client_connection(client_socket):
     with client_socket:
         while True:
            all = func(1, 2,client_socket)
            values[0]=str(twodp(all[0]/10))
            values[1]=str(twodp(all[1]/10))
           
            row = [values[0]] + [values[1]]
            if all[0]/10>25:
                alert(values[0])
                sendSms(values[0])
            print(formatter.format(*row), end = '\r')
            setData()
            time.sleep(120)

        
def twodp(val):
    return "%.2f" % round(val,2)

def func(address, to, client_socket):
    try:
        message = rtu.read_input_registers(1, address,to)
        rsp = send_message(message,client_socket)
        return rsp
    except Exception as e:
        print(e)
        pass
        
def setData():
    try:
        url = 'http://localhost:8033/incomes'
        headers = {'content-type': 'application/json'}
        jsonS = json.dumps({'temp': values[0],'hum': values[1]})
        r = requests.post(url, data=json.dumps(jsonS), headers=headers)

    except:
        print('error while posting to front')
        pass


def main():
    bind_ip = '0.0.0.0'
    bind_port = 5001

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
    	server.bind((bind_ip, bind_port))
    	server.listen(5)  # max backlog of connections

    	#print 'Listening on {}:{}'.format(bind_ip, bind_port)
    	sys.stdout.write('\n\n')

    	while True:
            client_sock, address = server.accept()
            print ('Accepted connection from {}:{}'.format(address[0], address[1]))
            client_handler = threading.Thread(
                target=handle_client_connection,
                args=(client_sock,)  # without comma you'd get a... TypeError: handle_client_connection() argument after * must be a sequence, not _socketobject
            )
            client_handler.start()

def test():
    alert(26.5)

if __name__ == "__main__":
    try:
       main()
    except Exception as e:
        print(e)
        sys.exit(0)

    #test()
