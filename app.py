from flask_socketio import SocketIO, emit
from flask import Flask, render_template, url_for, copy_current_request_context,request
from random import random
from time import sleep
from threading import Thread, Event

#from flask_cors import CORS


__author__ = 'sw'

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

#cors = CORS(app, resources={r"*": {"origins": "*"}})
socketio = SocketIO(app)

thread = Thread()

@app.route('/')
def index():
    
    #only by sending this page first will the client be connected to the socketio instance
    return render_template('index.html')

@app.route('/incomes', methods=['POST'])
def add_income():
    incomes=(request.get_json())
    socketio.emit('newnumber', {'number1': incomes}, namespace='/thermo')
    print('incomes',incomes)
    return '', 204

@socketio.on('connect', namespace='/thermo')
def test_connect():
    
    # need visibility of the global thread object
    global thread
    print('Client connected')
    
@socketio.on('disconnect', namespace='/thermo')
def test_disconnect():
    print('Client disconnected')



if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=8033)

